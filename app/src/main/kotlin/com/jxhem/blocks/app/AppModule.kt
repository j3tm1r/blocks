package com.jxhem.blocks.app

import android.app.Application
import android.content.Context
import android.support.annotation.NonNull
import com.jxhem.blocks.data.DataModule
import com.jxhem.blocks.di.ActivityBuilder
import com.jxhem.blocks.viewmodel.ViewModelModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(
    includes = arrayOf(
        ActivityBuilder::class,
        ViewModelModule::class,
        DataModule::class
    )
)
class AppModule {


    @Singleton
    @Provides
    @NonNull
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideExecutors(): AppExecutors {
        return AppExecutors()
    }
}

