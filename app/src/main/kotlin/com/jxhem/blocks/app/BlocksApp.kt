package com.jxhem.blocks.app

import com.jxhem.blocks.di.DaggerAppComponent

class BlocksApp : BaseApp() {
    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent
            .builder()
            .application(this)
            .build()
            .inject(this)
    }
}