package com.jxhem.blocks.ui.adapters

import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.jxhem.blocks.R
import com.jxhem.blocks.data.net.model.Model
import com.jxhem.blocks.databinding.TransactionItemBinding
import com.jxhem.blocks.databinding.TransactionViewBinding
import java.text.SimpleDateFormat
import java.util.*

class TransactionViewHolder(private val viewBinding: TransactionItemBinding) :
    RecyclerView.ViewHolder(viewBinding.root) {

    fun bind(
        transaction: Model.Tx,
        onItemSelectedListener: TransactionsAdapter.OnItemSelected
    ) {
        viewBinding.transaction = transaction
        viewBinding.root.setOnClickListener { onItemSelectedListener.onItemSelected(transaction.hash) }
    }

    companion object {
        fun create(parent: ViewGroup): TransactionViewHolder {
            val view = TransactionItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            return TransactionViewHolder(view)
        }
    }

}
