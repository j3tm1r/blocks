package com.jxhem.blocks.ui.view.mainview

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jxhem.blocks.data.source.Status
import com.jxhem.blocks.databinding.MainViewBinding
import com.jxhem.blocks.ui.NavigationProvider
import com.jxhem.blocks.ui.adapters.TransactionsAdapter
import com.jxhem.blocks.ui.view.BaseView
import javax.inject.Inject

class MainView : BaseView(), TransactionsAdapter.OnItemSelected {

    @Inject
    lateinit var navigationController: NavigationProvider

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainViewBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainViewBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MainViewModel::class.java)

        binding.swipeRefresh.setOnRefreshListener {
            loadData()
        }

        binding.transactionsList.adapter = TransactionsAdapter(emptyList(), this)

        setupTransactions()
        setupWallet()
    }

    override fun onItemSelected(hash: String) {
        navigationController.navigateToDetailView(hash)
    }

    private fun loadData() {
        viewModel.loadTransactions()
        viewModel.loadWallet()
    }


    private fun setupTransactions() {
        viewModel.getTransactions().observe(this, Observer {
            binding.swipeRefresh.isRefreshing = false
            when (it?.status) {
                Status.SUCCESS -> it.data?.let { it1 ->
                    (binding.transactionsList.adapter as TransactionsAdapter).setTransactions(it1)
                }
                Status.LOADING -> showMessage(binding.root, "Loading data...")
                else -> {
                    showMessageWithAction(
                        binding.root,
                        it?.message!!,
                        View.OnClickListener {
                            loadData()
                        }
                    )
                }
            }
        })
    }

    private fun setupWallet() {
        viewModel.getWallet().observe(this, Observer {
            when (it?.status) {
                Status.SUCCESS -> binding.wallet = it.data
                Status.LOADING -> {
                }
                else -> {
                    showMessageWithAction(
                        binding.root,
                        it?.message!!,
                        View.OnClickListener {
                            loadData()
                        }
                    )
                }
            }
        })

    }

    companion object {
        fun create(): MainView {
            return MainView()
        }
    }
}