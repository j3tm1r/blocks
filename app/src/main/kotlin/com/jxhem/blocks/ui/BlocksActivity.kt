package com.jxhem.blocks.ui

import android.os.Bundle
import com.jxhem.blocks.R
import javax.inject.Inject

class BlocksActivity : BaseActivity() {

    @Inject
    internal lateinit var mNavigationController: NavigationProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blocks)
        if (savedInstanceState == null)
            mNavigationController.navigateToMainView()
    }
}
