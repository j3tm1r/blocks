package com.jxhem.blocks.ui.view.mainview

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.jxhem.blocks.data.net.model.Model
import com.jxhem.blocks.data.source.BlocksDataSource
import com.jxhem.blocks.data.source.Resource
import com.jxhem.blocks.testing.OpenForTesting
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

@OpenForTesting
class MainViewModel
@Inject constructor(
    private val dataRepository: BlocksDataSource
) : ViewModel() {

    private val transactions: MutableLiveData<Resource<List<Model.Tx>>> = MutableLiveData()
    private val wallet: MutableLiveData<Resource<Model.Wallet>> = MutableLiveData()


    fun loadTransactions() {
        transactions.value = Resource.loading()
        dataRepository.getTransactions()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                transactions.value = Resource.success(it)
            }, {
                it.let { transactions.value = it.message?.let { it1 -> Resource.error(it1) } }
            })
    }

    fun getTransactions(): LiveData<Resource<List<Model.Tx>>> {
        return transactions
    }

    fun getWallet(): LiveData<Resource<Model.Wallet>> {
        return wallet
    }

    fun loadWallet() {
        wallet.value = Resource.loading()
        dataRepository.getWallet()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                wallet.value = Resource.success(it)
            }, {
                it.let { wallet.value = it.message?.let { it1 -> Resource.error(it1) } }
            })

    }
}