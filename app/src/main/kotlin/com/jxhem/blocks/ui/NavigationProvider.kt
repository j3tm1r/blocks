package com.jxhem.blocks.ui

import javax.inject.Inject

interface NavigationProvider {

    fun navigateToMainView()
    fun navigateToDetailView(id: String)
}