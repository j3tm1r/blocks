package com.jxhem.blocks.ui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.jxhem.blocks.R
import com.jxhem.blocks.ui.view.detailview.DetailView
import com.jxhem.blocks.ui.view.mainview.MainView

class NavigationController(baseActivity: BaseActivity) : NavigationProvider {
    private val containerId: Int = R.id.container
    private val fragmentManager: FragmentManager = baseActivity.supportFragmentManager


    override fun navigateToMainView() {
        var mainView = findFragment(MainView::class.java.simpleName) ?: MainView.create()

        fragmentManager
            .beginTransaction()
            .replace(containerId, mainView, MainView::class.java.simpleName)
            .setReorderingAllowed(true)
            .commitAllowingStateLoss()
    }

    private fun removeFragment(id: String) {
        var fragment = fragmentManager.findFragmentByTag(id)
        if (fragment != null) fragmentManager.beginTransaction().remove(fragment).commit()
    }

    private fun findFragment(id: String): Fragment? {
        return fragmentManager.findFragmentByTag(id)
    }

    override fun navigateToDetailView(id: String) {
        var detailView = findFragment(DetailView::class.java.simpleName) ?: DetailView.create(id)

        fragmentManager
            .beginTransaction()
            .add(containerId, detailView, DetailView::class.java.simpleName)
            .addToBackStack("")
            .setReorderingAllowed(true)
            .commitAllowingStateLoss()
    }
}