package com.jxhem.blocks.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.jxhem.blocks.data.net.model.Model

class TransactionsAdapter(
    private var transactions: List<Model.Tx>,
    private val onItemSelectedListener: OnItemSelected
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface OnItemSelected {
        fun onItemSelected(hash: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TransactionViewHolder.create(parent)
    }

    fun setTransactions(transactions: List<Model.Tx>) {
        this.transactions = transactions
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return transactions.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? TransactionViewHolder)?.bind(transactions[position], onItemSelectedListener)
    }
}
