package com.jxhem.blocks.ui.view.detailview

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.jxhem.blocks.data.net.model.Model
import com.jxhem.blocks.data.source.BlocksDataSource
import com.jxhem.blocks.data.source.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val dataRepository: BlocksDataSource
) : ViewModel() {


    private val transaction: MutableLiveData<Resource<Model.Tx>> = MutableLiveData()

    fun loadTransaction(id: String): LiveData<Resource<Model.Tx>> {

        dataRepository.getTransaction(id)
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe({
                transaction.postValue(Resource.success(it))
            }, {
                it.let { it.message.let { it?.let { it1 -> Resource.error<Model.Tx>(it1) } } }
            })

        return transaction
    }

}