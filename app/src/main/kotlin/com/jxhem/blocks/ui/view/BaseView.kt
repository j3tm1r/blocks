package com.jxhem.blocks.ui.view

import android.arch.lifecycle.ViewModelProvider
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.View
import com.jxhem.blocks.R
import com.jxhem.blocks.di.Injectable
import javax.inject.Inject

abstract class BaseView : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    fun showMessage(view: View, message: String) {
        val snackbar = Snackbar
            .make(view, message, Snackbar.LENGTH_SHORT)
        snackbar.show()
    }

    fun showMessageWithAction(view: View, message: String, onClickListener: View.OnClickListener) {
        val snackbar = Snackbar
            .make(view, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.retry, onClickListener)
        snackbar.show()
    }
}