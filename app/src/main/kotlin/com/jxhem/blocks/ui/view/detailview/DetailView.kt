package com.jxhem.blocks.ui.view.detailview

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jxhem.blocks.data.source.Status
import com.jxhem.blocks.databinding.DetailViewBinding
import com.jxhem.blocks.ui.view.BaseView

class DetailView : BaseView() {

    private lateinit var binding: DetailViewBinding
    private lateinit var viewModel: DetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DetailViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(DetailViewModel::class.java)
        viewModel.loadTransaction(arguments!!.getString(TRANSACTION_ID_KEY)!!)
            .observe(this, Observer {
                when (it?.status) {
                    Status.SUCCESS -> binding.transaction = it.data
                    Status.LOADING -> showMessage(binding.root, "Loading data...")
                    else -> {
                        showMessage(
                            binding.root,
                            it?.message!!
                        )
                    }
                }
            })
    }


    companion object {
        const val TRANSACTION_ID_KEY = "trasaction_id"

        fun create(id: String): DetailView {
            val args = Bundle()
            args.putString(TRANSACTION_ID_KEY, id)
            val detailView = DetailView()
            detailView.arguments = args
            return detailView
        }
    }
}
