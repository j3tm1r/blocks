package com.jxhem.blocks.ui

import dagger.Module
import dagger.Provides


@Module
class BlocksActivityModule {

    @Provides
    fun provideBaseActivity(activity: BlocksActivity)
            : NavigationProvider = NavigationController(activity)
}