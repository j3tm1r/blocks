package com.jxhem.blocks.binding

import android.databinding.BindingAdapter
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.jxhem.blocks.data.net.model.Model
import java.text.SimpleDateFormat
import com.jxhem.blocks.R
import com.jxhem.blocks.data.net.MultiAddressService
import com.jxhem.blocks.databinding.InputViewBinding
import com.jxhem.blocks.databinding.OutViewBinding
import java.util.*

/**
 * Data Binding adapters specific to the app.
 */
object BindingAdapters {

    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }


    @JvmStatic
    @BindingAdapter("txInputs")
    fun bindTransactionInputs(viewGroup: ViewGroup, inList: List<Model.Input>?) {
        inList?.forEach {
            var inputView = InputViewBinding.inflate(LayoutInflater.from(viewGroup.context))
            inputView.input = it
            viewGroup.addView(inputView.root)
        }
    }


    @JvmStatic
    @BindingAdapter("txOuts")
    fun bindTransactionOutputs(viewGroup: ViewGroup, outList: List<Model.Out>?) {
        outList?.forEach {
            // The out change is ignored
            if (it.xpub?.m == MultiAddressService.DEFAULT_XPUB) {
                var outView = OutViewBinding.inflate(LayoutInflater.from(viewGroup.context))
                outView.out = it
                viewGroup.addView(outView.root)
            }
        }
    }

    /**
     * Transforms a timestamp into a human readable format.
     * [timestamp] seconds since epoch
     */
    @JvmStatic
    @BindingAdapter("timestamp")
    fun formatTime(view: TextView, timestamp: Long) {
        var timeAsString = ""
        try {
            val sdf = SimpleDateFormat("hh:mm:ss dd/MM/yyyy")
            val netDate = Date(timestamp * 1000)
            timeAsString = sdf.format(netDate)
        } catch (e: Exception) {
            e.toString()
        }
        timeAsString = String.format(
            view.context.getString(R.string.transaction_time),
            timeAsString
        )

        view.text = timeAsString
    }

    @JvmStatic
    @BindingAdapter("addsIncome")
    fun addsIncome(view: TextView, addsIncome: Boolean) {
        view.setTextColor(
            if (addsIncome)
                ContextCompat.getColor(view.context, R.color.greenIn)
            else
                ContextCompat.getColor(view.context, R.color.redOut)
        )
    }

    @JvmStatic
    @BindingAdapter("srcBasedOnTransaction")
    fun srcBasedOnTransaction(view: ImageView, addsIncome: Boolean) {
        view.setImageResource(
            if (addsIncome) R.drawable.down_arrow
            else R.drawable.up_arrow
        )
    }

}
