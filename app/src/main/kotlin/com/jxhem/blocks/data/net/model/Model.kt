package com.jxhem.blocks.data.net.model

object Model {

    data class MultiAddressResponse(
        val recommend_include_fee: Boolean,
        val sharedcoin_endpoint: String,
        val info: Info,
        val wallet: Wallet,
        val addresses: List<Address>,
        val txs: List<Tx>
    )

    data class Info(
        val nconnected: Int,
        val conversion: Double,
        val symbol_local: SymbolLocal,
        val symbol_btc: SymbolBtc,
        val latest_block: LatestBlock
    )

    data class Wallet(
        val n_tx: Int,
        val n_tx_filtered: Int,
        val total_received: Long,
        val total_sent: Long,
        val final_balance: Long
    )


    data class Address(
        val address: String,
        val n_tx: Int,
        val total_received: Long,
        val total_sent: Long,
        val final_balance: Long,
        val gap_limit: Int,
        val change_index: Int,
        val account_index: Int
    )

    data class Tx(
        val hash: String,
        val ver: Int,
        val fee: Long,
        val vin_sz: Int,
        val vout_sz: Int,
        val size: Int,
        val relayed_by: String,
        val lock_time: Long,
        val tx_index: Int,
        val double_spend: Boolean,
        val result: Long,
        val balance: Long,
        val time: Long,
        val block_height: Long,
        val inputs: List<Input>,
        val out: List<Out>
    ) {
        fun addsIncome(): Boolean = result > 0
    }

    data class Input(
        val prev_out: PrevOut,
        val sequence: Long,
        val script: String
    )

    data class Out(
        val value: Long,
        val tx_index: Long,
        val n: Int,
        val spent: Boolean,
        val script: String,
        val type: Int,
        val addr: String,
        val xpub: Xpub?
    )

    data class PrevOut(
        val value: Long,
        val tx_index: Long,
        val n: Int,
        val spent: Boolean,
        val script: String,
        val type: Int,
        val addr: String,
        val xpub: Xpub
    )

    data class SymbolLocal(
        val code: String,
        val symbol: String,
        val name: String,
        val conversion: Double,
        val symbol_appears_after: Boolean,
        val local: Boolean
    )

    data class SymbolBtc(
        val code: String,
        val symbol: String,
        val name: String,
        val conversion: Double,
        val symbol_appears_after: Boolean,
        val local: Boolean
    )

    data class LatestBlock(
        val block_ndex: Long,
        val hash: String,
        val height: Long,
        val time: Long
    )

    data class Xpub(
        val m: String,
        val path: String
    )

}