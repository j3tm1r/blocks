package com.jxhem.blocks.data.net

import com.jxhem.blocks.data.net.model.Model
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MultiAddressService {

    @GET("multiaddr")
    fun getTransactions(@Query("active") xpub: String): Single<Model.MultiAddressResponse>


    companion object {

        val BASE_URL = " https://blockchain.info/"
        val DEFAULT_XPUB =
            "xpub6CfLQa8fLgtouvLxrb8EtvjbXfoC1yqzH6YbTJw4dP7srt523AhcMV8Uh4K3TWSHz9oDWmn9MuJogzdGU3ncxkBsAC9wFBLmFrWT9Ek81kQ"
    }
}
