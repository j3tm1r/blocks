package com.jxhem.blocks.data.source

import com.jxhem.blocks.data.net.model.Model
import io.reactivex.Observable

interface BlocksDataSource {

    fun getTransactions(): Observable<List<Model.Tx>>
    fun getTransaction(hash: String): Observable<Model.Tx>
    fun getWallet(): Observable<Model.Wallet>
}