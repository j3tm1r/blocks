package com.jxhem.blocks.data.repository

import com.jxhem.blocks.data.net.MultiAddressService
import com.jxhem.blocks.data.net.model.Model
import com.jxhem.blocks.data.source.BlocksDataSource
import io.reactivex.Observable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit


class BlocksNetworkRepository(
    private val multiAddressService: MultiAddressService
) : BlocksDataSource {

    override fun getTransaction(hash: String): Observable<Model.Tx> {
        var transaction: Model.Tx? = null
        transactions.forEach {
            if (it.hash.equals(hash))
                transaction = it
        }
        return Observable.just(transaction)
    }

    private var transactionsPublisher: PublishSubject<List<Model.Tx>> = PublishSubject.create()
    private var walletPublisher: PublishSubject<Model.Wallet> = PublishSubject.create()
    private lateinit var transactions: List<Model.Tx>

    override fun getWallet(): Observable<Model.Wallet> {
        return walletPublisher.take(1)
    }

    override fun getTransactions(): Observable<List<Model.Tx>> {
        updateTransactions()
        return transactionsPublisher.take(1)
    }

    private fun updateTransactions() {
        RxJavaPlugins.setErrorHandler { e -> }
        transactionsPublisher = PublishSubject.create()
        walletPublisher = PublishSubject.create()
        multiAddressService
            .getTransactions(MultiAddressService.DEFAULT_XPUB)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .timeout(30, TimeUnit.SECONDS)
            .subscribe({
                transactions = it.txs
                transactionsPublisher.onNext(it.txs)
                walletPublisher.onNext(it.wallet)
            }, {
                transactionsPublisher.onError(Throwable(it))
                walletPublisher.onError(Throwable(it))
            })

    }

}