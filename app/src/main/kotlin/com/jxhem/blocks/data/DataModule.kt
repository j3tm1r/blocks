package com.jxhem.blocks.data

import android.content.Context
import android.content.SharedPreferences
import com.jxhem.blocks.data.net.MultiAddressService
import com.jxhem.blocks.data.repository.BlocksNetworkRepository
import com.jxhem.blocks.data.source.BlocksDataSource
import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class DataModule {

    val PRODUCTION_API_URL: HttpUrl = HttpUrl.parse(MultiAddressService.BASE_URL)!!

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("blocks", Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideBlocksDataSource(): BlocksDataSource {
        return BlocksNetworkRepository(provideMultiAddressService())
    }

    private fun provideRetrofit(baseUrl: HttpUrl, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }


    fun provideBaseUrl(): HttpUrl {
        return PRODUCTION_API_URL
    }

    fun provideOkHttpClient(): OkHttpClient {
        return createApiClient()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

            // setup cache .cache(directory, size)
            //add interceptors / authenticators here
            .build()
    }

    fun provideMultiAddressService(): MultiAddressService {
        return provideRetrofit(provideBaseUrl(), provideOkHttpClient())
            .create(MultiAddressService::class.java)
    }

    private fun createApiClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
    }
}