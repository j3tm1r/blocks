package com.jxhem.blocks.di

import android.app.Application
import com.jxhem.blocks.app.AppModule
import com.jxhem.blocks.app.BlocksApp
import com.jxhem.blocks.ui.BlocksActivityModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        AndroidInjectionModule::class,
        AppModule::class
    )
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }

    fun inject(app: BlocksApp)
}