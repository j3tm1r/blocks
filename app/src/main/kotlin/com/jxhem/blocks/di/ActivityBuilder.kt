package com.jxhem.blocks.di

import com.jxhem.blocks.ui.BlocksActivity
import com.jxhem.blocks.ui.BlocksActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(
    includes = arrayOf(
        BlocksActivityModule::class
    )
)
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = arrayOf(
            FragmentBuildersModule::class,
            BlocksActivityModule::class
        )
    )
    abstract fun contributeBlocksActivity(): BlocksActivity

}