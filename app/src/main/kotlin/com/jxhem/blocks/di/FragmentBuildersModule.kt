package com.jxhem.blocks.di


import com.jxhem.blocks.ui.view.detailview.DetailView
import com.jxhem.blocks.ui.view.mainview.MainView
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeMainView(): MainView


    @ContributesAndroidInjector
    abstract fun contributeDetailView(): DetailView
}
