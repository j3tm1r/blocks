package com.jxhem.blocks.utils

object Utils {

    /**
     * Helper function to convet Satoshis to BTC.
     */
    @JvmStatic
    fun satoshisToBTC(input: Long): Double {
        return if (input == 0L) {
            0.0
        } else {
            input / Math.pow(10.0, 8.0)
        }
    }
}