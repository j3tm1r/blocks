package com.jxhem.blocks.viewmodel


import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.jxhem.blocks.ui.view.detailview.DetailViewModel
import com.jxhem.blocks.ui.view.mainview.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun bindDetailViewModel(mainViewModel: DetailViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
