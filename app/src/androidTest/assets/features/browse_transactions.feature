Feature: BrowseTransactions
    Visualize the transaction detail for one of the transactions

    Background:

    Scenario Outline: No transactions loaded
        Given I have a SingleFragmentActivity
        When I create a MainView
        And There are no transactions loaded
        Then The load hint is <visible>
        And I should not be in the DetailView

    Examples:
        |visible|
        |true|

    Scenario Outline: No transactions loaded with error
        Given I have a SingleFragmentActivity
        When I feed to the MainView a bad repository
        And There are no transactions loaded
        And The load hint is <visible>
        When I swipe on the transactions_list
        Then The load hint is <visible>
        And The error <message> is <visible>

    Examples:
        |visible| message|
        |true| dataError |

    Scenario Outline: Transactions loaded
        Given I have a SingleFragmentActivity
        And I create a MainView
        When I swipe on the transactions_list
        Then The load hint is <visible>
        And The transactions have been loaded

    Examples:
        |visible|
        |false|

    Scenario Outline: Open detail with transactions loaded
        Given I have a SingleFragmentActivity
        And I create a MainView
        And I have a MainView
        And I swipe on the transactions_list
        And The transactions have been loaded
        When I click on card at <position>
        Then I should be in the DetailView
        And I should see the transaction with hash <hash>

    Examples:
        | position | visible | hash                                                          | gone |
        | 0        | true | 4ff579235c965f58180dbe47cd6b66a72a5703ef5d32e0f095bc8194982c4417 | false|
        | 5        | true | 73500aea50c594a3bb4a437ef7ea2f869d5334845eed50b6d61503fd725fc801 | false|
