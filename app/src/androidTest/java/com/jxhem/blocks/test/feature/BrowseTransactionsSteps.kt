package com.jxhem.blocks.test.feature

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.swipeDown
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import com.jxhem.blocks.R
import com.jxhem.blocks.data.net.model.Model
import com.jxhem.blocks.data.repository.MockRepository
import com.jxhem.blocks.data.source.BlocksDataSource
import com.jxhem.blocks.test.util.ViewModelUtil
import com.jxhem.blocks.test.utils.RecyclerViewAssertions.adapterItemCountEquals
import com.jxhem.blocks.testing.SingleFragmentActivity
import com.jxhem.blocks.ui.NavigationProvider
import com.jxhem.blocks.ui.view.detailview.DetailView
import com.jxhem.blocks.ui.view.detailview.DetailViewModel
import com.jxhem.blocks.ui.view.mainview.MainView
import com.jxhem.blocks.ui.view.mainview.MainViewModel
import cucumber.api.CucumberOptions
import cucumber.api.java.After
import cucumber.api.java.Before
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import io.reactivex.Observable
import io.reactivex.Single
import junit.framework.Assert.assertNotNull
import net.bytebuddy.asm.Advice
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.junit.Rule


@CucumberOptions(
    features = arrayOf("features"),
    glue = arrayOf("com.jxhem.blocks.test")
)
class BrowseTransactionsSteps {

    @Rule
    var singleFragmentActivityTestRule =
        ActivityTestRule(SingleFragmentActivity::class.java, true, true)

    private lateinit var singleFragmentActivity: SingleFragmentActivity
    private val mockRepository: MockRepository = MockRepository()

    private lateinit var mainView: MainView

    @Before
    fun setup() {

    }

    @After
    fun tearDown() {

        singleFragmentActivityTestRule.activity?.finish()
    }

    @Given("^I have a SingleFragmentActivity")
    fun I_have_a_SingleFragmentActivity() {
        singleFragmentActivity = singleFragmentActivityTestRule.launchActivity(Intent())
        assertNotNull(singleFragmentActivity)
    }

    @Given("^I create a MainView")
    fun I_create_a_MainView() {
        mainView = MainView.create()
        mainView.navigationController = object : NavigationProvider {
            override fun navigateToMainView() {
                TODO("not implemented")
            }

            override fun navigateToDetailView(id: String) {
                val detailView = DetailView.create(id)
                detailView.viewModelFactory =
                        ViewModelUtil.createFor(DetailViewModel(mockRepository))
                singleFragmentActivity.setFragment(detailView)
            }

        }
        mainView.viewModelFactory = ViewModelUtil.createFor(MainViewModel(mockRepository))
        singleFragmentActivity.setFragment(mainView)
    }


    @Given("^I feed to the MainView a bad repository")
    fun I_create_a_MainView_with_a_bad_repository() {
        mainView = MainView.create()
        mainView.navigationController = object : NavigationProvider {
            override fun navigateToMainView() {
                TODO("not implemented")
            }

            override fun navigateToDetailView(id: String) {
                val detailView = DetailView.create(id)
                detailView.viewModelFactory =
                        ViewModelUtil.createFor(DetailViewModel(mockRepository))
                singleFragmentActivity.setFragment(detailView)
            }

        }
        mainView.viewModelFactory = ViewModelUtil.createFor(
            MainViewModel(object : BlocksDataSource {
                override fun getTransactions(): Observable<List<Model.Tx>> {
                    return Observable.error<List<Model.Tx>>(Throwable("dataError"))
                }

                override fun getTransaction(hash: String): Observable<Model.Tx> {
                    TODO("not implemented")
                }

                override fun getWallet(): Observable<Model.Wallet> {
                    return Observable.error<Model.Wallet>(Throwable("dataError"))
                }

            })
        )
        singleFragmentActivity.setFragment(mainView)
    }

    @When("^I have a MainView")
    fun I_have_a_MainView() {
        assertNotNull(findFragmentByTag(MainView::class.java.simpleName))
    }

    @When("^The MainView is visible")
    fun The_MainView_is_visible() {
        mainView = findFragmentByTag(MainView::class.java.simpleName) as MainView
        assert(mainView.isVisible)
    }

    @When("^There are no transactions loaded")
    fun no_transactions_loded() {
        onView(ViewMatchers.withId(R.id.transactions_list)).check(adapterItemCountEquals(0))
    }

    @When("^I swipe on the transactions_list")
    fun loadTransactions() {
        onView(withId(R.id.swipe_refresh)).perform(swipeDown())
    }

    @When("^I click on card at (\\d+)\$")
    fun I_click_on_a_transaction_card(position: Int) {
        onView(ViewMatchers.withId(R.id.transactions_list))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    position, click()
                )
            )
    }


    @Then("^The transactions have been loaded")
    fun The_transactions_have_been_loaded() {
        onView(ViewMatchers.withId(R.id.transactions_list))
            .check(adapterItemCountEquals(mockRepository.transactionsList.size))
    }

    @Then("^I should be in the DetailView")
    fun I_should_be_in_the_detail_view() {
        assertNotNull(findFragmentByTag(DetailView::class.java.simpleName))
        assert(findFragmentByTag(DetailView::class.java.simpleName)?.isVisible ?: false)
        val detailView = findFragmentByTag(DetailView::class.java.simpleName) as DetailView
        detailView.viewModelFactory = ViewModelUtil.createFor(DetailViewModel(mockRepository))
    }


    @Then("^I should see the transaction with hash (\\S+)\$")
    fun Then_I_should_see_the_transaction_with_hash(hash: String) {
        onView(withId(R.id.hash)).check(matches(withText(hash)))
    }


    @Then("^The load hint is (true|false)\$")
    fun Then_The_load_hint_is_visible(visible: Boolean) {
        if (visible)
            onView(withId(R.id.no_items)).check(matches(isDisplayed()))
        else
            onView(withId(R.id.no_items)).check(matches(not(isDisplayed())))

    }

    @Then("^The error (\\S+) is (true|false)\$")
    fun The_error_message_is_visible(message: String, visible: Boolean) {
        onView(withId(android.support.design.R.id.snackbar_text)).check(matches(isDisplayed()))

    }


    @Then("^I should not be in the DetailView")
    fun I_should_not_be_in_the_DetailView() {
        assert(findFragmentByTag(DetailView::class.java.simpleName)?.isVisible == false)
        // I should still be in the main view
        assert(findFragmentByTag(MainView::class.java.simpleName)?.isVisible == true)
    }


    private fun findFragmentByTag(tag: String): Fragment? {
        return singleFragmentActivity.supportFragmentManager?.findFragmentByTag(tag)

    }
}
