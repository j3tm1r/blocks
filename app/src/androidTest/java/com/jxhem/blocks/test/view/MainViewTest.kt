package com.jxhem.blocks.test.view

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.swipeDown
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.util.Log
import com.jxhem.blocks.R
import com.jxhem.blocks.data.repository.MockRepository
import com.jxhem.blocks.ui.BlocksActivity
import com.jxhem.blocks.ui.view.mainview.MainViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainViewTest {
    @Rule
    @JvmField
    val rule = ActivityTestRule<BlocksActivity>(BlocksActivity::class.java)


    private val mockRepository: MockRepository =
        MockRepository()
    private val mainViewModel: MainViewModel =
        MainViewModel(mockRepository)

    @Before
    fun initialize() {

    }

    @Test
    fun loaded() {
        Log.e("@Test", "Performing loading test")
        onView(withId(R.id.swipe_refresh)).perform(swipeDown())
    }

    @After
    fun clean() {

    }
}