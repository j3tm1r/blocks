# README #

I decided to implement this application following the MVVM pattern by using Fragments as Views, 
Android's Architecture ViewModels and LiveData. RxJava is used to fetch the data from the network with the help of 
Retrofit and OkHttp. 

I used the official DataBinding library to perform the binding between views and data coming from ViewModels.

The whole application is wired with Dagger 2, that is responsible for providing the dependencies as needed. 

Some testing/documentation is provided through the usage of cucumber as a form of introduction to Behaviour Driven Development.
To run the cucumber tests simply select the "CucumberTests" run configuration from the Android Studio dropdown menu or the by issuing  ./gradlew connectedCheck on the command line. 
